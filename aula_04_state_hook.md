# Aula 04 - State Hook

Consulte a video aula gravada e os materiais de leitura abaixo:

- [React Hooks](https://react.dev/reference/react);
- [State Hook](https://react.dev/learn/state-a-components-memory#);
- [Funções Puras](https://react.dev/learn/keeping-components-pure);
- [Componente TextInput](https://reactnative.dev/docs/textinput);

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-04-state-hook
