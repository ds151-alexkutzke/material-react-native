# Aula 12 - Bare workflow e Publicação

Consulte a video aula gravada e os materiais de leitura abaixo:

- [Manage Workflow X Bare Workflow](https://docs.expo.io/introduction/managed-vs-bare/):
  - [Limitações](https://docs.expo.io/introduction/why-not-expo/); 
  - [Expo Eject](https://docs.expo.io/workflow/customizing/);
  - [Como o Expo funciona](https://docs.expo.io/guides/how-expo-works/);
- [Publicação de Apps com Expo](https://docs.expo.io/distribution/introduction/):
  - [expo publish](https://docs.expo.io/workflow/expo-cli/#expo-publish);
  - [Release Channels](https://docs.expo.io/distribution/release-channels/);
  - [Testes em modo de produção](https://docs.expo.io/workflow/development-mode/);
- [Build de Aplicações Standalone](https://docs.expo.io/distribution/building-standalone-apps/):
  - [expo build](https://docs.expo.io/workflow/expo-cli/#expo-buildandroid);
  - [Login na plataforma Expo](https://docs.expo.io/build/setup/#2-log-in-to-your-expo-account);
  - [Updates para Aplicações Publicadas - OTA (Over-The_Air)](https://docs.expo.io/bare/updating-your-app/);
  - [Otimização de Updates](https://docs.expo.io/distribution/optimizing-updates/);
- Publicação em Lojas:
  - [Best practices para deploy em Lojas de Apps](https://docs.expo.io/distribution/app-stores/);
  - [App signing](https://docs.expo.io/distribution/app-signing/);
  - [Upload de Apps para Lojas](https://docs.expo.io/distribution/uploading-apps/);