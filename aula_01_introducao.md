# Aula 01 - Introdução

## Plano de ensino e avaliação

Consultar moodle da disciplina (https://ufprvirtual.ufpr.br).

## O que aprenderemos?

Como desenvolver aplicativos para Android (e iOS) utilizando o framework React Native.

## Como aprenderemos?

Por meio de aulas teóricas e práticas.

## Essa disciplina é difícil?

Não. Porém o bom aproveitamento depende de muita prática. O aprendizado do
desenvolvimento de aplicativos para dispositivos móveis utilizando
React Native envolve, entre outras coisas,
o estudo da linguagem Javascript moderna, bibliotecas e frameworks **atualizados com muita frequência**.

Portanto, é necessário praticar para que os exemplos sejam apreendidos e
aprendidos de forma adequada. Não se assuste com a quantidade de códigos novos
no início da disciplina. A ideia é que eles se tornem, pouco a pouco, mais
"palatáveis".

## Desenvolvimento para Dispositivos Móveis

Boa parte do desenvolvimento de aplicações para dispositivos móveis se direciona, 
hoje, a dois sistemas operacionais: **Android** (*Google*) e **iOS** (*Apple*).

### Android

- Desenvolvido por um consórcio de empresas conhecido como Open Handset Alliance, sendo o principal colaborador o Google;
- Sistema Operacional para dispositivos móveis mais utilizado em todo o mundo:
  - Mais de 2bi de dispositivos ativos;
- Além do sistema operacional, inclui:
  - Middleware (comunicação entre aplicativos);
  - Aplicações-chave;
- Kernel do Linux como base;
- Open-source ( http://source.android.com ):
  - Licença Apache;
  - Cada fabricante pode criar seu Android "personalizado";
  - Entretanto, para ter Apps do Google, o sistema deve ser homologado;
- Primeira versão lançada em 2008;
- **API Level** é um número sequencial que identifica a versão do Android
  (também possui o nome de um doce);
- Estamos indo para a API Level 35, versão 15:
  - Para saber mais: https://en.wikipedia.org/wiki/Android_version_history 
- É importante conhecer as versões do Android para sabermos quais as APIs,
  classes e recursos estão disponíveis para nossos aplicativos em certos
  aparelhos;

### iOS

- Criado e desenvolvido pela Apple Inc.
- Segundo Sistema Operacional para dispositivos móveis mais utilizado no mundo:
  - 13,7% do mercado;
- Além do sistema operacional, inclui:
  - Middleware (comunicação entre aplicativos);
  - Aplicações-chave;
- Código fechado com algumas partes Open-source:
  - Licença Apache;
- Primeira versão lançada em 2007;
- Revolucionou o conceito de smartphones e suas interfaces;

### Desenvolvimento Nativo

Entende-se por desenvolvimento nativo aquele que produz um software capaz de ser executando seguindo as especificações do Sistema Operacional e Hardware alvo.

Em geral, isso impõem que o software seja desenvolvido a partir de ferramentas e linguagens de programação específicos. 

A vantagem do desenvolvimento nativo é que o software final comunica-se diretamente com o sistema operacional, tendo acesso a bibliotecas oficiais e aos módulos do hardware. Tudo isso sem perdas adicionais de desempenho. No caso de smartphones, o aplicativo nativo tem acesso facilitado, por exemplo, aos dispositivos do hardware, como câmera, GPS, e outros, além de manter a experiência do usuário uniforme entre diferentes aplicativos.

### Desenvolvimento Não Nativo

Por outro lado, o desenvolvimento "não nativo" produz um software que não é capaz de rodar diretamente no sistema alvo. Ou seja, precisa ser, de alguma forma, emulado ou auxiliado por um outro software, este nativo, dentro do sistema.

Talvez uma das formas mais conhecidas de desenvolvimento de aplicativos não nativos seja através do Framework [Ionic](https://ionicframework.com/). Com ele, pode-se desenvolver aplicações web que são executadas dentro de um *WebView* nativo e simulam a experiência nativa de outros aplicativos. Ou seja, o aplicativo, na verdade, é uma página web apresentada por um navegador. Entretanto, este fato fica oculto para o usuário. Além disso, o framework auxilia os desenvolvedores com diversos módulos para facilitar o acesso aos dispositivos de hardware disponíveis.

### Desenvolvimento Cross-plataform

Desenvolvimento *Cross-plataform* é, por sua vez, o desenvolvimento de uma única aplicação que seja capaz de ser executada em diferentes plataformas. No caso dessa disciplina, o desenvolvimento *cross-plataform* permite a execução de um mesmo aplicativo tanto em dispositivos Android quanto iOS.

Diferentes frameworks permitem a criação de aplicativos *cross-plataform*. O próprio Ionic, descrito acima, é capaz de produzir, a partir de um mesmo código-fonte, aplicações para Android e iOS (afinal, é uma página web (sendo muito simplista aqui)).

Nessa disciplina utilizaremos o framework chamado [**React Native**](https://reactnative.dev/) que auxilia na produção de aplicações cross-plataform e nativas (sim! mas com suas ressalvas).

### Estudaremos algo cross-plataform, mas nem tanto ...

Embora a tecnologia a ser utilizada por nós permite o desenvolvimento para dispositivos Android e iOS, essa disciplina terá um enfoque mais direto no desenvolvimento para Android.

Isso se deve a limitações de hardware impostas ao desenvolvimento para dispositivos iOS, mesmo utilizando React Native. 

Em outras palavras, não possuo um computador Apple. Até tenho um velhinho, mas ele roda Archlinux btw. :)

Se você possui um computador da Apple, fique à vontade para utilizá-lo. Inclusive, compartilhe com a turma seus resultados. Eu apenas não conseguirei demonstrar o desenvolvimento para essa plataforma durante as aulas.

## Ambiente de desenvolvimento

Será interessante, portanto, que todos possuam o Android Studio instalado em suas máquina.

Estritamente, o desenvolvimento de aplicações com React Native (mais especificamente com o expo)
não exige a instalação do Android Studio. Entretanto, para testes mais expressivos e uma experiência mais completa, sua instalação é interessante.

Abaixo estão algumas informações sobre tecnologias e softwares que utilizaremos durante as aulas.

### Javascript
Linguagem de programação que utilizaremos por toda a disciplina.

É importante notar que a linguagem Javascript possui muitas versões e é atualizada quase anualmente. Por isso, utilizaremos vários recursos novos da linguagem. Para saber mais sobre as versões existentes e suas especificidades, consulte a Seção de leituras indicadas.

### NodeJS

É um runtime de Javascript. Ou seja, um ambiente capaz de executar código Javascript fora dos navegadores de internet. Possibilita, assim, o uso dessa linguagem para a criação de aplicações completas de propósito geral, e não apenas scripts dentro de navegadores e páginas web.

### NPM (ou yarn)

"Node package manager" é uma aplicação de controle e instalação de pacotes e bibliotecas Javascript. Utilizaremos durante a disciplina para a instalação de dependências durante o desenvolvimento de aplicativos.

O "Yarn" é uma alternativa ao "NPM" que pode ser utilizada durante a disciplina. Entretanto, todos os exemplos demonstrados nesse material terá o "NPM" como gerenciador de pacotes oficial. Além disso, não é aconselhável o uso dos dois softwares ao mesmo tempo. Conflitos podem ocorrer.

### Expo-cli

É uma das aplicações contidas no framework "Expo Tools". Este framework auxilia (muito!) na criação de aplicações com React Native. Considere um Rails para React Native. :)

Falaremos mais sobre o "Expo Tools" nas próximas aulas.

### Editor de texto

O próprio site do React Native indica o uso do ~~Vim~~ VScode como editor de texto de preferência. Porém, fique à vontade para escolher o seu.

### Teste de uma primeira aplicação

Para a instalação do ambiente necessário e uma demonstração de uma primeira aplicação, para teste desse ambiente, consulte o seguinte link:

https://reactnative.dev/docs/environment-setup

Consulte também a primeira vídeo aula da disciplina.

## Passo a passo

Inicialmente, devemos instalar as aplicações necessárias para nosso ambiente de desenvolvimento.

Primeiro, vamos instalar o Android Studio (embora não seja estritamente necessária quando utilizamos o expo-cli):

https://developer.android.com/studio/

Após, é preciso instalar o NodeJS. Para isso, consulte as instruções do seu sistema operacional ou [utilize uma ferramenta do tipo *Version Manager*](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm). Nesse semestre, utilizarei a versão 16.15 (LTS).

A indicação para a disciplina é instalar o [NVM](https://github.com/nvm-sh/nvm) e utilizar a verão LTS do nodejs:

```
nvm install --lts
nvm use --lts
```

Se a instalação estiver correta, você também terá acesso ao comando `npm`, que será o gerenciador de pacotes Node que utilizaremos na disciplina (você também pode utilizar o [Yarn](https://yarnpkg.com/) se preferir).

Em versões recentes, o React Native indicava o uso do comando abaixo para a criação de novas aplicações:

```
### Versão depreciada
expo init AwesomeProject --npm
cd AwesomeProject
expo start          # ou apenas npm start
```

Atualmente, a indicação é fazer uso da aplicação `create-expo-app`, através do `npx`:

```
npx create-expo-app AwesomeProject

cd AwesomeProject
npx expo start
```

Selecionar a opção `blank`.

O ambiente de desenvolvimento irá iniciar. Pressione `j` para abrir a interface web.

A interface disponibiliza várias formas de executar a aplicação (Emulador Android, Emulador iOS, dispositivo Android real ou web browser).

Para executar em um dispositivo Android real, baixe o aplicativo chamado "ExpoGO". Através da aplicação, escaneie o QR Code exibido pelo Expo.

Em caso de problemas de conexão, tente inicializar o Expo com um "tunnel", selecionando essa opção pela interface web ou inicializando a aplicação com o comando `npx expo start --tunnel`.

Investigue e edite o arquivo `App.js`. Perceba que a aplicação é atualizada automaticamente.

Para utilizar um emulador Android, utilize a interface do Android Studio para criá-lo. Lembre-se de iniciar o emulador antes de rodar `expo start` no seu projeto. Isso poupa alguns problemas.

## Referências e leituras indicadas

- [Sobre o Javascript e suas versões](https://scotch.io/courses/10-need-to-know-javascript-concepts/js-versions-ecmascript-and-javascript)
- [Uma re-introdução ao Javascript](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/A_re-introduction_to_JavaScript)
- [NodeJS](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)
- [Yarn](https://yarnpkg.com/)
- [Expo-cli](https://docs.expo.io/workflow/expo-cli/)
- [Versões Android](https://en.wikipedia.org/wiki/Android_version_history)
- [Comparação entre S.O.s mobile](https://en.wikipedia.org/wiki/Comparison_of_mobile_operating_systems)
- [Sobre o Android](https://en.wikipedia.org/wiki/Android_(operating_system))
- [Sobre o iOS](https://en.wikipedia.org/wiki/IOS)
- [React](https://reactjs.org/)
- [React Native](https://reactnative.dev/)
- [VS](https://www.vim.org/)[Code](https://code.visualstudio.com/)
