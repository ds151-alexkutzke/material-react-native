# Conceitos Principais

## Hello World

- O menor exemplo de React:

```react
ReactDOM.render(
  <h1>Hello, world!</h1>,
  document.getElementById('root')
);
```

## Introdução ao JSX

- JSX é uma extensão de sintaxe do JavaScript;
- Permite a inclusão de objetos xml diretamente no código:
  - Esses objetos serão convertidos para código JS (geralmente react);
- Não é apenas uma linguagem de template:
  - Pode-se utilizar todo o poder do JS;

### Por que JSX?

- React aceita o fato de que a lógica de renderização é intrinsecamente ligada a lógica de interface:
  - Ao invés de separar tecnologias com marcação e lógica em arquivos separados, opta-se por separar interesses (*concerns*);
  - Para isso, criam-se unidades chamadas **componentes** com marcação e lógica, mas separados de outros componentes;
- JSX não é obrigatório:
  - Tudo pode ser escrito em plain JavaScript;
  - Mas é indicado;

### Incluindo expressões no JSX

- Utiliza-se chaves: `{...}`:

```react
const name = 'Josh Perez';
const element = <h1>Hello, {name}</h1>;
ReactDOM.render(
  element,
  document.getElementById('root')
);
```
- Dentro das chaves, pode-se incluir qualquer código JS;
- Quando escrever código JSX em mais de uma linha, utilize parênteses para evitar a inclusão de automática de ";" (mas convenhamos, quem utilizaria um editor de texto que adicionar código automaticamente, não é mesmo?);
- Expressões JSX são convertidos para código JS após a compilação:
  - Portanto, podem ser utilizados em qualquer momento;
  - Dentro de `if`, `for` e etc;

### Especificando Atributos com JSX

- Para especificar atributos literais em expressões JSX, **utilize aspas**:

```react
const element = <div tabIndex="0"></div>;
```

- Para expressões JS, **utilize apenas chaves, sem aspas**:

```react
const element = <img src={user.avatarUrl}></img>;
```

- **Importante**: convenção de nomes para JSX é diferente do HTML no que diz respeito aos atributos:
  - Utiliza-se `camelCase`;
  - `class` fica `className`;
  - `tabindex` fica `tabIndex`;

### Especificando elementos filhos com JSX

- Tags vazias podem terminar com `/>`:

```react
const element = <img src={user.avatarUrl} />;
```

- Tags JSX podem ter tags filhas:

```react
const element = (
  <div>
    <h1>Hello!</h1>
    <h2>Good to see you here.</h2>
  </div>
);
```

### JSX previne ataques de Injeção (XSS)

- Todas os valores passados para expressões JSX são **escapados** e convertidos para strings antes de serem utilizados;
- Previne ataques do tipo XSS (cross-site-scripting);

### JSX representa objetos

- O compilador `Babel` torna expressões JSX em chamadas `React.createElement()` (nota: novas versões do React terão uma transformação diferente, que nem sempre utilizará `.createElement`, tornando-as mais rápidas. Mais infos, ver [aqui](https://reactjs.org/blog/2020/09/22/introducing-the-new-jsx-transform.html)):

```react
const element = (
  <h1 className="greeting">
    Hello, world!
  </h1>
);
```

- É idêntico ao seguinte:

```react
const element = React.createElement(
  'h1',
  {className: 'greeting'},
  'Hello, world!'
);
```

- `React.createElement()` retorna objetos que chamamos de **Elementos React**:

```react
// Note: this structure is simplified
const element = {
  type: 'h1',
  props: {
    className: 'greeting',
    children: 'Hello, world!'
  }
};
```

- Esses elementos possuem informações sobre o que a tela deve informar ao usuário;
- O React, vai então, renderizá-los no DOM.

### Renderizando Elementos
