# Instalação

## Iniciando

- **React** é uma biblioteca JavaScript para construção de interfaces de usuários:
  - Pode-se consultar o tutorial: https://reactjs.org/tutorial/tutorial.html
- Desenvolvido para adoção gradual:
  - Utilizar o quanto quiser do React no seus projetos;
- Opções para aprender:
  - Brincar com online code playgrouds: CodePen, CodeSandbox, Glutch ou Stackblitz, por exemplo;
  - HTML para testar: https://raw.githubusercontent.com/reactjs/reactjs.org/master/static/html/single-file-example.html
    - Não serve para produção:
      - Compila o JSX com o Babel no navegador;
      - Utiliza o React para Dev;
      - Lento!
  - Adicionar o React a um projeto já existente:
    - https://reactjs.org/docs/add-react-to-a-website.html
  - Tutorial prático: https://reactjs.org/tutorial/tutorial.html
  - Documentação: https://reactjs.org/docs/hello-world.html
  - Introdução mais lenta: https://www.taniarascia.com/getting-started-with-react/
  - **Importante**: (re)Introdução ao Javascript: https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript
  - Cursos: https://reactjs.org/community/courses.html

## Adicionar React a um Website

- Adicione um "container":
  - Pode ser uma div com um id:

```html
<!-- ... existing HTML ... -->

<div id="like_button_container"></div>

<!-- ... existing HTML ... -->
```

- Adicione 3 scripts:
  - React, react-dom e seus componentes:

```html
  <!-- Load React. -->
  <!-- Note: when deploying, replace "development.js" with "production.min.js". -->
  <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>  <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
  <!-- Load our React component. -->
  <script src="like_button.js"></script>
```

- Crie o código do seu componente:
  - No exemplo acima, seria `like_button.js`;

```jsx
'use strict';

const e = React.createElement;

class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { liked: false };
  }

  render() {
    if (this.state.liked) {
      return 'You liked this.';
    }

    return e(
      'button',
      { onClick: () => this.setState({ liked: true }) },
      'Like'
    );
  }
}

const domContainer = document.querySelector('#like_button_container');
ReactDOM.render(e(LikeButton), domContainer);
```

- Para produção, o próximo passo seria minificar seu JS e carregar os arquivos React de `production.min.js`:
  - Para minificar, consulte "terser";

### Utilize JSX

- Acima, foi utilizado apenas recursos nativos do JS;
- É possível utilizar o JSX para adicionar notação "XML" ao JS;
- Forma rápida de utilizar o JSX direto no navegador (lento):

```html
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
```

- Na tag `<script>` utilize `type="text/babel"`;
- Para produção, é necessário utilizar um JSX preprocessor (para converter tudo para JS nativo):

```bash
npm init -y
npm install babel-cli@6 babel-preset-react-app@3
```

- Para uma pasta `src` com seu código, podemos executar:

```bash
npx babel --watch src --out-dir . --presets react-app/prod 
```

- `npx` executa diretamente pacotes do npm;
- O comando vai iniciar um watcher para a pasta `src`;
- O Babel auxilia inclusive na compatibilidade com navegadores antigos:
  - Vai converter tudo para rodar nativamente em versões antigas do JS;

## Crie uma nova aplicação React

- Apresenta toolchains para trabalhar com React;

- **Create React App**:  Aplicação para criar uma estrutura de app com React (mostly single-page app):

```bash
npx create-react-app my-app
cd my-app
npm start
```

- **Next.js**: framework para páginas com React. Assume uso de Node.js no servidor. Traz soluções de *styling* *routing*. Legal para server-rendered apps.
 
- **Gatsby**: Para criação de sites estáticos com React. Gera HTML e CSS pré-renderizados para acelerar o load;

- Outros exemplos: Neutrino, Nx, Parcel, Razzle.

### Criar um toolchain do zero

- Em geral, toolchains são compostos de:
  - Package manager: yarn, npm, ...
  - Bundle: webpack, parcel, ... Une códigos em bundles;
  - Compiler: Babel, ... Reescreve códigos (em geral JS).

## CDN Links

```html
<script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
```

```html
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
```

- Versão pode ser especificada no `@16`;
