# Aula 06 - Navigation e Axios

Consulte a video aula gravada e os materiais de leitura abaixo:

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect


## React Navigation

- [Biblioteca React Navigation](https://reactnavigation.org/);
  - [Instalação](https://reactnavigation.org/docs/getting-started);
  - [Configuração inicial](https://reactnavigation.org/docs/hello-react-navigation);
  - [Navegação simples](https://reactnavigation.org/docs/navigating);

```
npm install @react-navigation/native
expo install react-native-screens react-native-safe-area-context
npm install @react-navigation/native-stack
```

- [Commit: 23adda1b](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/23adda1b44517919150a9116b461c7d70cb72cfd): Instala react-navigation (stack) e altera App.js;
- [Commit: 0e0943c7](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/0e0943c718a51cc13f7aac9637832173e1cf8dd6): Cria tela HomeScreen;

## Expo Vector Icons

- [Expo vector icons](https://docs.expo.io/guides/icons/):
  - [Lista de ícones](https://icons.expo.fyi/);

- [Commit e4037a0c](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/e4037a0cd1ba37d2b352e1e05723dd8e09db982a): Adiciona Expo Vector Icons (Feather)

## Implementação SearchBar

- [Commit: 281aea65](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/281aea655f213e3dc9c1f67dcb6b812e95face90): Inicializa componente SearchBar;
- [Commit: faa6be57](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/faa6be5734a392191b2b6ed7c19d651cecb30108): Adiciona estilo para SearchBar;

## The MovieDB

- [The Movie Database API](https://developers.themoviedb.org/3/);

## Dot Env

- [React Native DotEvn](https://www.npmjs.com/package/react-native-dotenv);

```
npm install react-native-dotenv
```

## Biblioteca Axios

- [Biblioteca Axios](https://github.com/axios/axios):
  - [Instalação](https://github.com/axios/axios#installing);
  - [Criação de instância](https://github.com/axios/axios#creating-an-instance);

```
npm install axios
```

- [Commit: ee588e0a](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/ee588e0a1d05a3c3288c63b45cd71c28e6e54437): Inicializa axios e implementa e testa searchTmdb (não esquece de setar sua API_KEY em .env.local);
- [Commit: 7c7a0347](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/7c7a03477553a4f7d9d8ee4442d62636a186e99a): Eventos para executar busca;
- [Commit: 961fa6db](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/961fa6db3cc0b4cf7e3989e382882b3dfc1a5a28): Conclui implementação da busca por filmes;

## useEffect

- [Effect Hook](https://reactjs.org/docs/hooks-effect.html);
- [Commit: b2c30ea7](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/b2c30ea7232bf6b751b025e7fbbf165c6a6b9770): Exemplo useEffect para primeira renderização da tela;

## React Fragments

- [React Fragments](https://reactjs.org/docs/fragments.html);
- [Commit: eeb4a454](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/eeb4a454955d4eaf138e57eca8b382b531515e28): Exemplo problema Scroll + View;
- [Commit: e022fe21](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/e022fe21c248790656c2831a24b3321ebfe5d021): Solução com React Fragment;

## Navegação com parâmetros

- [Passagem de parâmetros para telas](https://reactnavigation.org/docs/params);
- [Commit: 6b3f6702](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/6b3f6702e209f8a24ef627e332b33377865203de): Navegação entre telas com parâmetros;
- [Commit: 0cce3431](https://gitlab.com/ds151-alexkutzke/code-06-navigation-axios-effect/-/commit/0cce343169f7a4ae370f8125500747d1c3d62af8): Navegação com parâmetros e requisição;
