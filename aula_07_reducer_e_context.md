# Aula 07 - Reducer Hook e Context

Consulte a video aula gravada e os materiais de leitura abaixo:

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-07-reducer-context

## Reducer

- [Sobre Reducers](https://react.dev/learn/extracting-state-logic-into-a-reducer)
  - [`useReducer`](https://react.dev/reference/react/useReducer);

- [Commit: 3303432b](https://gitlab.com/ds151-alexkutzke/code-07-reducer-context/-/commit/3303432b727209dc2926eb91785599f689dcc436): implementa countReducer;

## Context

- [Sobre Contextos](https://react.dev/learn/passing-data-deeply-with-context):
  - [`useContext`](https://react.dev/reference/react/useContext);


* [Commit: 1175647d](https://gitlab.com/ds151-alexkutzke/code-07-reducer-context/-/commit/1175647d385af66ce2abf18ebec88ec626e4736f): cria contexto TestContext;
* [Commit: f0e51f50](https://gitlab.com/ds151-alexkutzke/code-07-reducer-context/-/commit/f0e51f50dac0947781fc3e5836555112affe4c73): Implementa TestContext com useState;


## Context com Reducer

- [Commit: 6dbbcede](https://gitlab.com/ds151-alexkutzke/code-07-reducer-context/-/commit/6dbbcede144e518cf3ffd70a5f02cb78c656d446): Implementa CounterContext com Reducer;
- [Commit: 8c5646f2](https://gitlab.com/ds151-alexkutzke/code-07-reducer-context/-/commit/8c5646f295a195420f1f3433d2fa328b70c282f1): Implementa CounterContext mais genérico, com api;

