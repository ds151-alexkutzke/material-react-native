# Aula 03 - Conceitos iniciais de RN

Consulte a video aula gravada e os materiais de leitura abaixo:

- [Introdução ao RN](https://reactnative.dev/docs/getting-started);
- [Core Components e Componentes Nativos](https://reactnative.dev/docs/intro-react-native-components); 
- [Uso da `FlatList`](https://reactnative.dev/docs/using-a-listview);
- Componentes (referência):
  - [`Text`](https://reactnative.dev/docs/text);
  - [`View`](https://reactnative.dev/docs/view);
  - [`FlatList`](https://reactnative.dev/docs/flatlist);
  - [`Button`](https://reactnative.dev/docs/button);
  - [`TouchableOpacity`](https://reactnative.dev/docs/touchableopacity);
  - [`Image`](https://reactnative.dev/docs/image);

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-03-conceitos-iniciais

## Como funciona o RN?

- [React Native em 2019, nova arquitetura e comparações com Flutter](https://blog.rocketseat.com.br/react-native-em-2019/);
- Para uma leitura mais aprofundada:
  - [React Native — A Bridge To Project Fabric — Part 1](https://medium.com/swlh/react-native-a-bridge-to-project-fabric-part-1-5af6a53c0d83);
  - [React Native — A Bridge To Project Fabric — Part 2](https://medium.com/@chenfeldmn/react-native-a-bridge-to-project-fabric-part-2-1f082415b881);
  - [React Native — A Bridge To Project Fabric — Part 3](https://medium.com/@chenfeldmn/react-native-a-bridge-to-project-fabric-part-3-ae495794c6b0);
- [React Native's New Architecture - Parashuram N - React Conf 2018](https://www.youtube.com/watch?v=UcqRXTriUVI).
