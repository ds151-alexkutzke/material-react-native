# Aula 09 - Mais sobre React Navigation

Consulte a video aula gravada e os materiais de leitura abaixo:

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-09-navigation

## Navegadores

- [Navegador Stack](https://reactnavigation.org/docs/stack-navigator);
- [Navegador Native Stack](https://reactnavigation.org/docs/native-stack-navigator);
- [Navegadoe Drawer](https://reactnavigation.org/docs/drawer-navigator);
- [Navegador Material-Top-Tab](https://reactnavigation.org/docs/material-top-tab-navigator);

* [Commit: 96b3af52](https://gitlab.com/ds151-alexkutzke/code-09-navigation/-/commit/96b3af52450a3ef1c6e75d720e4df8bbc28c5a05): instala drawer e material-top-tabs e configura drawer.

### Dependências para Drawer 

```bash
npm install @react-navigation/drawer
expo install react-native-gesture-handler react-native-reanimated
```

Além disso, é preciso adicionar um plugin ao arquivo `babel.config.js`:

```js
  module.exports = {
      ...
      plugins: [
          ...
          'react-native-reanimated/plugin',
      ],
  };
```

Após, limpar o cache com o comando abaixo:

```bash
expo start -c
```

Mais informações sobre a instalação do pacote `react-native-reanimated`: https://docs.swmansion.com/react-native-reanimated/docs/fundamentals/installation#babel-plugin

### Dependências para Material-Top-Tab

```bash
npm install @react-navigation/material-top-tabs react-native-tab-view
expo install react-native-pager-view
```

## Navegação Aninhada

- [Navegação Aninhada](https://reactnavigation.org/docs/nesting-navigators);

* [Commit: e0b0d627](https://gitlab.com/ds151-alexkutzke/code-09-navigation/-/commit/e0b0d627f18e791f5c8776f10364304912334c49): cria navegação aninhada.

## Outros Temas Importantes

- [`useNavigation`](https://reactnavigation.org/docs/use-navigation) e [Navegação sem a Propriedade `navigation`](https://reactnavigation.org/docs/navigating-without-navigation-prop/);
- [Ciclo de Vida](https://reactnavigation.org/docs/navigation-lifecycle);
- [Eventos de Navegação](https://reactnavigation.org/docs/navigation-events);
- [Safe Areas](https://reactnavigation.org/docs/handling-safe-area/);

## React Native Elements

```bash
npm install @rneui/themed @rneui/base
```

- [React Native Elements](https://reactnativeelements.com/):
  - [Customização](https://reactnativeelements.com/docs/customizing);

* [Commit: 0acde51a](https://gitlab.com/ds151-alexkutzke/code-09-navigation/-/commit/0acde51a4f95db1f80366a8be94dd74af2c823d0): adiciona SeachBar.

## React Native Debugging

- [Debugging](https://reactnative.dev/docs/debugging);
