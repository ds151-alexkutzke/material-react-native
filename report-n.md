# Entrega dos exercícios
## Turma: DS151 1/2021 - Noite

**Atualizado em: 2021-11-18 14:09:26 -0300**

* **grr20150224 - ADRIANA CORREA RODRIGUES ds151-2021-1-N-grr20150224**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-09-30T23:30:03.697Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20164542 - ALLANA DE ALMEIDA FLAUSINO ds151-2021-1-N-grr20164542**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20175466 - AMANDA REGINA DE OLIVEIRA MAZUROK ds151-2021-1-N-grr20175466**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 8 commits :)<br>Criado em: 2021-09-26T17:58:37.564Z<br>Atualizado em: 2021-10-03T00:16:14.076Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 6 commits :)<br>Criado em: 2021-10-07T00:25:53.129Z<br>Atualizado em: 2021-11-04T17:28:02.802Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 4 commits :)<br>Criado em: 2021-10-27T12:54:37.010Z<br>Atualizado em: 2021-11-02T18:35:45.603Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-11-16T23:20:42.307Z
* **grr20184626 - BRIAN LUCAS FERREIRA DA ROCHA `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20190286 - CESAR HENRIQUE XAVIER DE SOUZA ds151-2021-1-N-grr20190286**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-10-02T23:01:21.258Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-10-31T01:40:40.526Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-11-13T13:46:56.825Z
* **grr20184579 - CHRISTIAN DUECK ds151-2021-1-N-grr20184579**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 3 commits :)<br>Criado em: 2021-09-22T12:09:07.255Z<br>Atualizado em: 2021-10-06T14:41:19.842Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-07T16:20:21.796Z<br>Atualizado em: 2021-11-17T12:37:22.871Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 2 commits :)<br>Criado em: 2021-10-27T16:41:36.928Z<br>Atualizado em: 2021-11-04T02:37:48.271Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 1 commit :)<br>Criado em: 2021-11-04T14:28:01.573Z<br>Atualizado em: 2021-11-17T20:49:07.252Z
* **grr20184588 - CLERITON ADRIANO MUCHINSKI ds151-2021-1-N-grr20184588**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-10-03T00:48:24.728Z<br>Atualizado em: 2021-11-04T11:40:15.888Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-15T01:01:14.335Z<br>Atualizado em: 2021-11-05T01:33:29.841Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 5 commits :)<br>Criado em: 2021-10-19T18:35:11.765Z<br>Atualizado em: 2021-11-08T13:53:37.033Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 3 commits :)<br>Criado em: 2021-11-11T22:16:50.943Z<br>Atualizado em: 2021-11-17T20:47:29.065Z
* **grr20163138 - DANIEL HENRIQUE SEPULVEDA HIROOKA `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20168186 - DIEGO DE MORAES SANTOS `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20193869 - EDUARDO HENRIQUE DE PAULA MORO ds151-2021-1-N-grr20193869**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 8 commits :)<br>Criado em: 2021-10-07T19:57:40.878Z<br>Atualizado em: 2021-10-08T00:46:14.471Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 4 commits :)<br>Criado em: 2021-10-15T00:08:54.500Z<br>Atualizado em: 2021-11-04T18:11:44.306Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20204431 - ENZO TOMAZ RADEL `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20170244 - ERICK RAMPIM GARCIA ds151-2021-1-N-grr20170244**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 8 commits :)<br>Criado em: 2021-09-27T17:12:05.097Z<br>Atualizado em: 2021-10-02T21:38:32.901Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-19T16:29:22.410Z<br>Atualizado em: 2021-11-04T18:14:06.125Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 3 commits :)<br>Criado em: 2021-11-03T10:44:30.037Z<br>Atualizado em: 2021-11-05T00:21:59.120Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 1 commit :)<br>Criado em: 2021-11-16T14:49:32.060Z<br>Atualizado em: 2021-11-18T01:47:32.226Z
* **grr20158734 - EVERLY ANA DOS SANTOS `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20166717 - FELIPE DE MELO BECEL `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20193861 - GABRIEL BISCAIA FERREIRA `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20206123 - GABRIEL FUMES BAETA `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20193738 - GABRIEL SCHMIDT WOJCIECHOWSKI ds151-2021-1-N-grr20193738**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 3 commits :)<br>Criado em: 2021-09-30T17:55:15.188Z<br>Atualizado em: 2021-10-07T22:24:23.701Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 2 commits :)<br>Criado em: 2021-10-18T01:38:35.423Z<br>Atualizado em: 2021-10-20T00:45:49.902Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 1 commit :)<br>Criado em: 2021-10-31T22:58:36.632Z<br>Atualizado em: 2021-11-04T20:29:00.811Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 2 commits :)<br>Criado em: 2021-11-14T18:31:00.689Z<br>Atualizado em: 2021-11-15T20:15:10.501Z
* **grr20184611 - GIULIA GUARISE GUTIERREZ ds151-2021-1-N-grr20184611**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-10-04T02:38:48.155Z<br>Atualizado em: 2021-10-04T02:38:48.155Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-11T21:08:39.940Z<br>Atualizado em: 2021-10-12T21:39:51.072Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 4 commits :)<br>Criado em: 2021-10-17T23:12:54.248Z<br>Atualizado em: 2021-11-04T20:30:49.260Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 1 commit :)<br>Criado em: 2021-11-18T01:51:36.507Z<br>Atualizado em: 2021-11-18T01:51:36.507Z
* **grr20184551 - ISRAEL DE QUADROS ds151-2021-1-N-grr20184551**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-18T19:40:25.939Z<br>Atualizado em: 2021-11-04T18:18:33.456Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20193771 - JOSÉ ADILSON DE PAULA CARDOSO ds151-2021-1-N-grr20193771**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 2 commits :)<br>Criado em: 2021-10-07T21:23:45.297Z<br>Atualizado em: 2021-10-08T00:26:38.874Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-20T18:36:48.032Z<br>Atualizado em: 2021-11-04T18:23:28.881Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 1 commit :)<br>Criado em: 2021-11-03T00:13:34.026Z<br>Atualizado em: 2021-11-04T20:33:44.678Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 2 commits :)<br>Criado em: 2021-11-17T21:05:12.630Z<br>Atualizado em: 2021-11-18T00:43:03.804Z
* **grr20193750 - JÚLIA RENATA ENDLER FIGUEIREDO ds151-2021-1-N-grr20193750**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-10-08T02:25:33.265Z<br>Atualizado em: 2021-11-04T18:59:36.358Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-10-21T16:51:54.365Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 1 commit :)<br>Criado em: 2021-10-21T16:56:00.054Z<br>Atualizado em: 2021-11-04T20:39:53.064Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 2 commits :)<br>Criado em: 2021-11-17T04:14:00.540Z<br>Atualizado em: 2021-11-18T00:52:53.743Z
* **grr20185325 - LAERTE SOUZA COSTA NETO `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20184597 - LEONARDO CASTRO DE OLIVEIRA ds151-2021-1-N-grr20184597**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Exercício entregue fora do prazo =/<br>Criado em: 2021-10-08T22:20:14.472Z<br>Atualizado em: 2021-10-09T00:56:20.424Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-19T23:48:51.635Z<br>Atualizado em: 2021-10-20T02:10:39.467Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 1 commit :)<br>Criado em: 2021-11-03T21:11:29.000Z<br>Atualizado em: 2021-11-04T20:43:05.428Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 1 commit :)<br>Criado em: 2021-11-17T20:29:10.972Z<br>Atualizado em: 2021-11-18T02:31:58.533Z
* **grr20204597 - LEONARDO GIOVANI SANDRINI ds151-2021-1-N-grr20204597**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 5 commits :)<br>Criado em: 2021-09-27T02:58:36.522Z<br>Atualizado em: 2021-09-27T02:58:36.522Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 4 commits :)<br>Criado em: 2021-10-11T07:35:53.962Z<br>Atualizado em: 2021-11-08T16:25:48.029Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 5 commits :)<br>Criado em: 2021-10-31T16:18:56.291Z<br>Atualizado em: 2021-11-08T16:31:44.948Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-11-15T22:47:27.861Z
* **grr20163134 - LEONARDO WROBEL ds151-2021-1-N-grr20163134**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Exercício entregue fora do prazo =/<br>Criado em: 2021-10-06T01:30:59.333Z<br>Atualizado em: 2021-10-27T00:51:19.478Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Exercício entregue fora do prazo =/<br>Criado em: 2021-10-28T23:36:28.343Z<br>Atualizado em: 2021-11-05T01:19:57.574Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-11-10T23:40:01.503Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20175509 - LIA DAIELY MAGALHÃES ds151-2021-1-N-grr20175509**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-10-07T01:11:10.566Z<br>Atualizado em: 2021-10-15T19:03:04.555Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 2 commits :)<br>Criado em: 2021-10-14T00:00:42.000Z<br>Atualizado em: 2021-11-04T18:34:53.015Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 4 commits :)<br>Criado em: 2021-10-26T19:51:45.010Z<br>Atualizado em: 2021-11-04T20:45:20.994Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-11-16T21:58:05.705Z
* **grr20196040 - LUKAS GUIBOR DOS SANTOS COSTA ds151-2021-1-N-grr20196040**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 6 commits :)<br>Criado em: 2021-10-04T17:29:41.370Z<br>Atualizado em: 2021-11-04T11:46:39.381Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 3 commits :)<br>Criado em: 2021-10-04T17:34:39.127Z<br>Atualizado em: 2021-10-15T11:08:21.831Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 2 commits :)<br>Criado em: 2021-11-01T22:05:29.000Z<br>Atualizado em: 2021-11-04T20:47:33.760Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20184576 - MARCOS ANTONIO PETRY KERCHNER ds151-2021-1-N-grr20184576**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Exercício entregue fora do prazo =/<br>Criado em: 2021-10-15T00:40:34.861Z<br>Atualizado em: 2021-11-04T11:50:55.409Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-20T01:10:46.380Z<br>Atualizado em: 2021-11-04T18:39:23.763Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 1 commit :)<br>Criado em: 2021-11-01T19:49:48.692Z<br>Atualizado em: 2021-11-04T20:54:36.732Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 1 commit :)<br>Criado em: 2021-11-16T23:46:44.277Z<br>Atualizado em: 2021-11-18T03:04:06.166Z
* **grr20175470 - MARCOS LIKIO NOGAWA ds151-2021-1-N-grr20175470**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-10-07T23:01:27.294Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20176923 - MARIANA MODESTO MIGUEL DA SILVA `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20176298 - MATHEUS AUGUSTO DE MORAES RIBEIRO ds151-2021-1-N-grr20176298**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 5 commits :)<br>Criado em: 2021-09-30T21:26:14.938Z<br>Atualizado em: 2021-10-03T17:05:06.721Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 4 commits :)<br>Criado em: 2021-10-07T22:39:06.841Z<br>Atualizado em: 2021-10-14T23:57:03.014Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 6 commits :)<br>Criado em: 2021-10-21T22:13:02.716Z<br>Atualizado em: 2021-11-04T21:00:59.487Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 4 commits :)<br>Criado em: 2021-11-04T21:37:28.691Z<br>Atualizado em: 2021-11-10T22:47:51.069Z
* **grr20200168 - MATHEUS CARDOZO CALDEIRA ds151-2021-1-N-grr20200168**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-10-08T00:04:57.812Z<br>Atualizado em: 2021-10-08T00:04:57.812Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-20T20:20:48.451Z<br>Atualizado em: 2021-10-20T20:20:48.451Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 3 commits :)<br>Criado em: 2021-11-03T16:40:53.472Z<br>Atualizado em: 2021-11-04T21:09:38.132Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20163234 - MATHEUS WARKENTIN ds151-2021-1-N-grr20163234**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-09-28T11:03:46.218Z<br>Atualizado em: 2021-09-28T11:03:46.218Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-06T00:45:40.722Z<br>Atualizado em: 2021-10-06T00:45:40.722Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 4 commits :)<br>Criado em: 2021-10-23T12:46:20.068Z<br>Atualizado em: 2021-11-04T21:12:22.887Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Fork, mas nenhum commit =/<br>Criado em: 2021-11-17T16:14:09.333Z
* **grr20193940 - NÍCKOLAS UKASINSKI ds151-2021-1-N-grr20193940**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-10-06T00:34:11.567Z<br>Atualizado em: 2021-10-06T00:34:11.567Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-17T23:45:15.064Z<br>Atualizado em: 2021-10-19T21:39:46.864Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 6 commits :)<br>Criado em: 2021-10-25T17:27:21.990Z<br>Atualizado em: 2021-10-27T19:15:15.129Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 1 commit :)<br>Criado em: 2021-11-15T19:17:53.901Z<br>Atualizado em: 2021-11-18T17:05:50.596Z
* **grr20193920 - NÍCOLAS TEIXEIRA GUERRA GARCIA ds151-2021-1-N-grr20193920**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-09-21T19:12:17.532Z<br>Atualizado em: 2021-09-25T14:19:18.117Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-09T14:06:02.101Z<br>Atualizado em: 2021-11-04T18:48:33.069Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 1 commit :)<br>Criado em: 2021-10-23T16:25:58.249Z<br>Atualizado em: 2021-10-31T04:20:47.664Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 3 commits :)<br>Criado em: 2021-11-04T00:50:15.827Z<br>Atualizado em: 2021-11-15T17:27:41.376Z
* **grr20195733 - PEDRO HENRIQUE CORDEIRO DE MACEDO `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20184635 - RAPHAEL PEREIRA RODRIGUES ds151-2021-1-N-grr20184635**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Exercício entregue fora do prazo =/<br>Criado em: 2021-10-15T04:28:48.847Z<br>Atualizado em: 2021-11-04T11:56:56.833Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 2 commits :)<br>Criado em: 2021-10-15T04:36:21.746Z<br>Atualizado em: 2021-11-04T18:50:34.053Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 2 commits :)<br>Criado em: 2021-11-03T00:16:54.118Z<br>Atualizado em: 2021-11-04T21:20:40.972Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 2 commits :)<br>Criado em: 2021-11-18T00:36:52.687Z<br>Atualizado em: 2021-11-18T04:24:32.961Z
* **grr20184584 - THYAGO VINICIUS CAMILO CUNHA ds151-2021-1-N-grr20184584**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> 1 commit :)<br>Criado em: 2021-10-06T01:07:36.305Z<br>Atualizado em: 2021-10-06T03:41:02.257Z
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> 3 commits :)<br>Criado em: 2021-11-16T01:00:32.912Z<br>Atualizado em: 2021-11-18T05:46:11.699Z
* **grr20167541 - TIAGO FRANCISCHINI `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20193846 - VICTOR HUGO FARIAS ds151-2021-1-N-grr20193846**
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> 1 commit :)<br>Criado em: 2021-10-19T14:10:03.259Z<br>Atualizado em: 2021-11-04T18:52:31.052Z
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> 1 commit :)<br>Criado em: 2021-11-02T23:54:26.122Z<br>Atualizado em: 2021-11-04T21:22:53.315Z
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(
* **grr20186256 - VITÓRIA GOMES BRITO `Grupo não encontrado!` **Reveja o nome do seu grupo.****
  1. **ds151-react-vote-assignment (Prazo: 2021-10-07)**:<br> Sem fork :(
  2. **ds151-task-list-assignment (Prazo: 2021-10-20)**:<br> Sem fork :(
  3. **ds151-tmdb-assignment (Prazo: 2021-11-04)**:<br> Sem fork :(
  4. **ds151-notes-assignment (Prazo: 2021-11-18)**:<br> Sem fork :(



|Login|Nome|ds151-react-vote-assignment (Prazo: 2021-10-07)|ds151-task-list-assignment (Prazo: 2021-10-20)|ds151-tmdb-assignment (Prazo: 2021-11-04)|ds151-notes-assignment (Prazo: 2021-11-18)|
|:-:|:-:|:-:|:-:|:-:|:-:|
|grr20150224|ADRIANA CORREA RODRIGUES ds151-2021-1-N-grr20150224|Fork, mas nenhum commit =/<br>Criado em: 2021-09-30T23:30:03.697Z|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20164542|ALLANA DE ALMEIDA FLAUSINO ds151-2021-1-N-grr20164542|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20175466|AMANDA REGINA DE OLIVEIRA MAZUROK ds151-2021-1-N-grr20175466|8 commits :)<br>Criado em: 2021-09-26T17:58:37.564Z<br>Atualizado em: 2021-10-03T00:16:14.076Z|6 commits :)<br>Criado em: 2021-10-07T00:25:53.129Z<br>Atualizado em: 2021-11-04T17:28:02.802Z|4 commits :)<br>Criado em: 2021-10-27T12:54:37.010Z<br>Atualizado em: 2021-11-02T18:35:45.603Z|Fork, mas nenhum commit =/<br>Criado em: 2021-11-16T23:20:42.307Z|
|grr20184626|BRIAN LUCAS FERREIRA DA ROCHA `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20190286|CESAR HENRIQUE XAVIER DE SOUZA ds151-2021-1-N-grr20190286|Fork, mas nenhum commit =/<br>Criado em: 2021-10-02T23:01:21.258Z|Sem fork :(|Fork, mas nenhum commit =/<br>Criado em: 2021-10-31T01:40:40.526Z|Fork, mas nenhum commit =/<br>Criado em: 2021-11-13T13:46:56.825Z|
|grr20184579|CHRISTIAN DUECK ds151-2021-1-N-grr20184579|3 commits :)<br>Criado em: 2021-09-22T12:09:07.255Z<br>Atualizado em: 2021-10-06T14:41:19.842Z|1 commit :)<br>Criado em: 2021-10-07T16:20:21.796Z<br>Atualizado em: 2021-11-17T12:37:22.871Z|2 commits :)<br>Criado em: 2021-10-27T16:41:36.928Z<br>Atualizado em: 2021-11-04T02:37:48.271Z|1 commit :)<br>Criado em: 2021-11-04T14:28:01.573Z<br>Atualizado em: 2021-11-17T20:49:07.252Z|
|grr20184588|CLERITON ADRIANO MUCHINSKI ds151-2021-1-N-grr20184588|1 commit :)<br>Criado em: 2021-10-03T00:48:24.728Z<br>Atualizado em: 2021-11-04T11:40:15.888Z|1 commit :)<br>Criado em: 2021-10-15T01:01:14.335Z<br>Atualizado em: 2021-11-05T01:33:29.841Z|5 commits :)<br>Criado em: 2021-10-19T18:35:11.765Z<br>Atualizado em: 2021-11-08T13:53:37.033Z|3 commits :)<br>Criado em: 2021-11-11T22:16:50.943Z<br>Atualizado em: 2021-11-17T20:47:29.065Z|
|grr20163138|DANIEL HENRIQUE SEPULVEDA HIROOKA `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20168186|DIEGO DE MORAES SANTOS `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20193869|EDUARDO HENRIQUE DE PAULA MORO ds151-2021-1-N-grr20193869|8 commits :)<br>Criado em: 2021-10-07T19:57:40.878Z<br>Atualizado em: 2021-10-08T00:46:14.471Z|4 commits :)<br>Criado em: 2021-10-15T00:08:54.500Z<br>Atualizado em: 2021-11-04T18:11:44.306Z|Sem fork :(|Sem fork :(|
|grr20204431|ENZO TOMAZ RADEL `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20170244|ERICK RAMPIM GARCIA ds151-2021-1-N-grr20170244|8 commits :)<br>Criado em: 2021-09-27T17:12:05.097Z<br>Atualizado em: 2021-10-02T21:38:32.901Z|1 commit :)<br>Criado em: 2021-10-19T16:29:22.410Z<br>Atualizado em: 2021-11-04T18:14:06.125Z|3 commits :)<br>Criado em: 2021-11-03T10:44:30.037Z<br>Atualizado em: 2021-11-05T00:21:59.120Z|1 commit :)<br>Criado em: 2021-11-16T14:49:32.060Z<br>Atualizado em: 2021-11-18T01:47:32.226Z|
|grr20158734|EVERLY ANA DOS SANTOS `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20166717|FELIPE DE MELO BECEL `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20193861|GABRIEL BISCAIA FERREIRA `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20206123|GABRIEL FUMES BAETA `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20193738|GABRIEL SCHMIDT WOJCIECHOWSKI ds151-2021-1-N-grr20193738|3 commits :)<br>Criado em: 2021-09-30T17:55:15.188Z<br>Atualizado em: 2021-10-07T22:24:23.701Z|2 commits :)<br>Criado em: 2021-10-18T01:38:35.423Z<br>Atualizado em: 2021-10-20T00:45:49.902Z|1 commit :)<br>Criado em: 2021-10-31T22:58:36.632Z<br>Atualizado em: 2021-11-04T20:29:00.811Z|2 commits :)<br>Criado em: 2021-11-14T18:31:00.689Z<br>Atualizado em: 2021-11-15T20:15:10.501Z|
|grr20184611|GIULIA GUARISE GUTIERREZ ds151-2021-1-N-grr20184611|1 commit :)<br>Criado em: 2021-10-04T02:38:48.155Z<br>Atualizado em: 2021-10-04T02:38:48.155Z|1 commit :)<br>Criado em: 2021-10-11T21:08:39.940Z<br>Atualizado em: 2021-10-12T21:39:51.072Z|4 commits :)<br>Criado em: 2021-10-17T23:12:54.248Z<br>Atualizado em: 2021-11-04T20:30:49.260Z|1 commit :)<br>Criado em: 2021-11-18T01:51:36.507Z<br>Atualizado em: 2021-11-18T01:51:36.507Z|
|grr20184551|ISRAEL DE QUADROS ds151-2021-1-N-grr20184551|Sem fork :(|1 commit :)<br>Criado em: 2021-10-18T19:40:25.939Z<br>Atualizado em: 2021-11-04T18:18:33.456Z|Sem fork :(|Sem fork :(|
|grr20193771|JOSÉ ADILSON DE PAULA CARDOSO ds151-2021-1-N-grr20193771|2 commits :)<br>Criado em: 2021-10-07T21:23:45.297Z<br>Atualizado em: 2021-10-08T00:26:38.874Z|1 commit :)<br>Criado em: 2021-10-20T18:36:48.032Z<br>Atualizado em: 2021-11-04T18:23:28.881Z|1 commit :)<br>Criado em: 2021-11-03T00:13:34.026Z<br>Atualizado em: 2021-11-04T20:33:44.678Z|2 commits :)<br>Criado em: 2021-11-17T21:05:12.630Z<br>Atualizado em: 2021-11-18T00:43:03.804Z|
|grr20193750|JÚLIA RENATA ENDLER FIGUEIREDO ds151-2021-1-N-grr20193750|1 commit :)<br>Criado em: 2021-10-08T02:25:33.265Z<br>Atualizado em: 2021-11-04T18:59:36.358Z|Fork, mas nenhum commit =/<br>Criado em: 2021-10-21T16:51:54.365Z|1 commit :)<br>Criado em: 2021-10-21T16:56:00.054Z<br>Atualizado em: 2021-11-04T20:39:53.064Z|2 commits :)<br>Criado em: 2021-11-17T04:14:00.540Z<br>Atualizado em: 2021-11-18T00:52:53.743Z|
|grr20185325|LAERTE SOUZA COSTA NETO `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20184597|LEONARDO CASTRO DE OLIVEIRA ds151-2021-1-N-grr20184597|Exercício entregue fora do prazo =/<br>Criado em: 2021-10-08T22:20:14.472Z<br>Atualizado em: 2021-10-09T00:56:20.424Z|1 commit :)<br>Criado em: 2021-10-19T23:48:51.635Z<br>Atualizado em: 2021-10-20T02:10:39.467Z|1 commit :)<br>Criado em: 2021-11-03T21:11:29.000Z<br>Atualizado em: 2021-11-04T20:43:05.428Z|1 commit :)<br>Criado em: 2021-11-17T20:29:10.972Z<br>Atualizado em: 2021-11-18T02:31:58.533Z|
|grr20204597|LEONARDO GIOVANI SANDRINI ds151-2021-1-N-grr20204597|5 commits :)<br>Criado em: 2021-09-27T02:58:36.522Z<br>Atualizado em: 2021-09-27T02:58:36.522Z|4 commits :)<br>Criado em: 2021-10-11T07:35:53.962Z<br>Atualizado em: 2021-11-08T16:25:48.029Z|5 commits :)<br>Criado em: 2021-10-31T16:18:56.291Z<br>Atualizado em: 2021-11-08T16:31:44.948Z|Fork, mas nenhum commit =/<br>Criado em: 2021-11-15T22:47:27.861Z|
|grr20163134|LEONARDO WROBEL ds151-2021-1-N-grr20163134|Exercício entregue fora do prazo =/<br>Criado em: 2021-10-06T01:30:59.333Z<br>Atualizado em: 2021-10-27T00:51:19.478Z|Exercício entregue fora do prazo =/<br>Criado em: 2021-10-28T23:36:28.343Z<br>Atualizado em: 2021-11-05T01:19:57.574Z|Fork, mas nenhum commit =/<br>Criado em: 2021-11-10T23:40:01.503Z|Sem fork :(|
|grr20175509|LIA DAIELY MAGALHÃES ds151-2021-1-N-grr20175509|1 commit :)<br>Criado em: 2021-10-07T01:11:10.566Z<br>Atualizado em: 2021-10-15T19:03:04.555Z|2 commits :)<br>Criado em: 2021-10-14T00:00:42.000Z<br>Atualizado em: 2021-11-04T18:34:53.015Z|4 commits :)<br>Criado em: 2021-10-26T19:51:45.010Z<br>Atualizado em: 2021-11-04T20:45:20.994Z|Fork, mas nenhum commit =/<br>Criado em: 2021-11-16T21:58:05.705Z|
|grr20196040|LUKAS GUIBOR DOS SANTOS COSTA ds151-2021-1-N-grr20196040|6 commits :)<br>Criado em: 2021-10-04T17:29:41.370Z<br>Atualizado em: 2021-11-04T11:46:39.381Z|3 commits :)<br>Criado em: 2021-10-04T17:34:39.127Z<br>Atualizado em: 2021-10-15T11:08:21.831Z|2 commits :)<br>Criado em: 2021-11-01T22:05:29.000Z<br>Atualizado em: 2021-11-04T20:47:33.760Z|Sem fork :(|
|grr20184576|MARCOS ANTONIO PETRY KERCHNER ds151-2021-1-N-grr20184576|Exercício entregue fora do prazo =/<br>Criado em: 2021-10-15T00:40:34.861Z<br>Atualizado em: 2021-11-04T11:50:55.409Z|1 commit :)<br>Criado em: 2021-10-20T01:10:46.380Z<br>Atualizado em: 2021-11-04T18:39:23.763Z|1 commit :)<br>Criado em: 2021-11-01T19:49:48.692Z<br>Atualizado em: 2021-11-04T20:54:36.732Z|1 commit :)<br>Criado em: 2021-11-16T23:46:44.277Z<br>Atualizado em: 2021-11-18T03:04:06.166Z|
|grr20175470|MARCOS LIKIO NOGAWA ds151-2021-1-N-grr20175470|Fork, mas nenhum commit =/<br>Criado em: 2021-10-07T23:01:27.294Z|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20176923|MARIANA MODESTO MIGUEL DA SILVA `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20176298|MATHEUS AUGUSTO DE MORAES RIBEIRO ds151-2021-1-N-grr20176298|5 commits :)<br>Criado em: 2021-09-30T21:26:14.938Z<br>Atualizado em: 2021-10-03T17:05:06.721Z|4 commits :)<br>Criado em: 2021-10-07T22:39:06.841Z<br>Atualizado em: 2021-10-14T23:57:03.014Z|6 commits :)<br>Criado em: 2021-10-21T22:13:02.716Z<br>Atualizado em: 2021-11-04T21:00:59.487Z|4 commits :)<br>Criado em: 2021-11-04T21:37:28.691Z<br>Atualizado em: 2021-11-10T22:47:51.069Z|
|grr20200168|MATHEUS CARDOZO CALDEIRA ds151-2021-1-N-grr20200168|1 commit :)<br>Criado em: 2021-10-08T00:04:57.812Z<br>Atualizado em: 2021-10-08T00:04:57.812Z|1 commit :)<br>Criado em: 2021-10-20T20:20:48.451Z<br>Atualizado em: 2021-10-20T20:20:48.451Z|3 commits :)<br>Criado em: 2021-11-03T16:40:53.472Z<br>Atualizado em: 2021-11-04T21:09:38.132Z|Sem fork :(|
|grr20163234|MATHEUS WARKENTIN ds151-2021-1-N-grr20163234|1 commit :)<br>Criado em: 2021-09-28T11:03:46.218Z<br>Atualizado em: 2021-09-28T11:03:46.218Z|1 commit :)<br>Criado em: 2021-10-06T00:45:40.722Z<br>Atualizado em: 2021-10-06T00:45:40.722Z|4 commits :)<br>Criado em: 2021-10-23T12:46:20.068Z<br>Atualizado em: 2021-11-04T21:12:22.887Z|Fork, mas nenhum commit =/<br>Criado em: 2021-11-17T16:14:09.333Z|
|grr20193940|NÍCKOLAS UKASINSKI ds151-2021-1-N-grr20193940|1 commit :)<br>Criado em: 2021-10-06T00:34:11.567Z<br>Atualizado em: 2021-10-06T00:34:11.567Z|1 commit :)<br>Criado em: 2021-10-17T23:45:15.064Z<br>Atualizado em: 2021-10-19T21:39:46.864Z|6 commits :)<br>Criado em: 2021-10-25T17:27:21.990Z<br>Atualizado em: 2021-10-27T19:15:15.129Z|1 commit :)<br>Criado em: 2021-11-15T19:17:53.901Z<br>Atualizado em: 2021-11-18T17:05:50.596Z|
|grr20193920|NÍCOLAS TEIXEIRA GUERRA GARCIA ds151-2021-1-N-grr20193920|1 commit :)<br>Criado em: 2021-09-21T19:12:17.532Z<br>Atualizado em: 2021-09-25T14:19:18.117Z|1 commit :)<br>Criado em: 2021-10-09T14:06:02.101Z<br>Atualizado em: 2021-11-04T18:48:33.069Z|1 commit :)<br>Criado em: 2021-10-23T16:25:58.249Z<br>Atualizado em: 2021-10-31T04:20:47.664Z|3 commits :)<br>Criado em: 2021-11-04T00:50:15.827Z<br>Atualizado em: 2021-11-15T17:27:41.376Z|
|grr20195733|PEDRO HENRIQUE CORDEIRO DE MACEDO `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20184635|RAPHAEL PEREIRA RODRIGUES ds151-2021-1-N-grr20184635|Exercício entregue fora do prazo =/<br>Criado em: 2021-10-15T04:28:48.847Z<br>Atualizado em: 2021-11-04T11:56:56.833Z|2 commits :)<br>Criado em: 2021-10-15T04:36:21.746Z<br>Atualizado em: 2021-11-04T18:50:34.053Z|2 commits :)<br>Criado em: 2021-11-03T00:16:54.118Z<br>Atualizado em: 2021-11-04T21:20:40.972Z|2 commits :)<br>Criado em: 2021-11-18T00:36:52.687Z<br>Atualizado em: 2021-11-18T04:24:32.961Z|
|grr20184584|THYAGO VINICIUS CAMILO CUNHA ds151-2021-1-N-grr20184584|1 commit :)<br>Criado em: 2021-10-06T01:07:36.305Z<br>Atualizado em: 2021-10-06T03:41:02.257Z|Sem fork :(|Sem fork :(|3 commits :)<br>Criado em: 2021-11-16T01:00:32.912Z<br>Atualizado em: 2021-11-18T05:46:11.699Z|
|grr20167541|TIAGO FRANCISCHINI `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
|grr20193846|VICTOR HUGO FARIAS ds151-2021-1-N-grr20193846|Sem fork :(|1 commit :)<br>Criado em: 2021-10-19T14:10:03.259Z<br>Atualizado em: 2021-11-04T18:52:31.052Z|1 commit :)<br>Criado em: 2021-11-02T23:54:26.122Z<br>Atualizado em: 2021-11-04T21:22:53.315Z|Sem fork :(|
|grr20186256|VITÓRIA GOMES BRITO `Grupo não encontrado!` **Reveja o nome do seu grupo.**|Sem fork :(|Sem fork :(|Sem fork :(|Sem fork :(|
