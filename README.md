Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

DS151 - Desenvolvimento para Dispositivos Móveis

Prof. Alexander Robert Kutzke

# Material da disciplina DS151 - Desenvolvimento para Dispositivos Móveis
# React Native

## Tarefas

* [React Vote](https://gitlab.com/ds151-alexkutzke/ds151-react-vote-assignment):
* [Task List](https://gitlab.com/ds151-alexkutzke/ds151-task-list-assignment):
* [TMDB](https://gitlab.com/ds151-alexkutzke/ds151-tmdb-assignment):
* [Notes](https://gitlab.com/ds151-alexkutzke/ds151-notes-assignment):
* [Gitlab](https://gitlab.com/ds151-alexkutzke/ds151-gitlab-assignment):
* [Maps](https://gitlab.com/ds151-alexkutzke/ds151-maps-assignment):

## Trabalho prático

* [2020 - Período Especial 2](trabalho_2020_especial_2.md)

## :sparkles: Galeria de Trabalhos Interessantes

* ds151-react-vote-assignment:
  * [André Vitor Kuduavski](https://gitlab.com/ds151-alexkutzke/gallery/ds151-react-vote-assignment-andre-vitor-uduavski);
  * [Carlos Felipe Godinho Silva](https://gitlab.com/ds151-alexkutzke/gallery/ds151-react-vote-assignment-carlos-felipe-godinho-silva);
  * [Cassiano Kruchelski](https://gitlab.com/ds151-alexkutzke/gallery/ds151-react-vote-assignment-cassiano-kruchelski);
  * [Julio do Lago Muller](https://gitlab.com/ds151-alexkutzke/gallery/ds151-react-vote-assignment-julio-do-lago-muller) - https://github.com/juliolmuller/;
* ds151-task-list-assignment:
  * [Cassiano Kruchelski](https://gitlab.com/ds151-alexkutzke/gallery/ds151-task-list-assignment-cassiano-kruchelski);
* ds151-tmdb-assignment:
  * [Cassiano Kruchelski](https://gitlab.com/ds151-alexkutzke/gallery/ds151-tmdb-assignment-cassiano-kruchelski);
  * [Julio do Lago Muller](https://gitlab.com/ds151-alexkutzke/gallery/ds151-tmdb-assignment-julio-do-lago-muller) - https://github.com/juliolmuller/;
* ds151-notes-assignment:
  * [Cassiano Kruchelski](https://gitlab.com/ds151-alexkutzke/gallery/ds151-notes-assignment-cassiano-kruchelski);
  * [Julio do Lago Muller](https://gitlab.com/ds151-alexkutzke/gallery/ds151-notes-assignment-julio-do-lago-muller) - https://github.com/juliolmuller/;
* ds151-gitlab-assignment:
  * [Cassiano Kruchelski](https://gitlab.com/ds151-alexkutzke/gallery/ds151-gitlab-assignment-cassiano-kruchelski);
  * [Julio do Lago Muller](https://gitlab.com/ds151-alexkutzke/gallery/ds151-gitlab-assignment-julio-do-lago-muller) - https://github.com/juliolmuller/;
  * [Wanderson Giacomin](https://gitlab.com/ds151-alexkutzke/gallery/ds151-gitlab-assignment-wanderson-giacomin);

## Aulas

- Aula 01 - [Apresentação da Disciplina](aula_01_introducao.md);
- Aula 02 - [React (Native)](aula_02_react.md);
- Aula 03 - [Conceitos Iniciais de RN](aula_03_conceitos_iniciais.md);
- Aula 04 - [State Hook](aula_04_state_hook.md);
- Aula 05 - [Layout](aula_05_layout.md);
- Aula 06 - [React Navigation, Axios e Effect Hooks](aula_06_navigation_e_axios_effect.md);
- Aula 07 - [Reducer e Context](aula_07_reducer_e_context.md);
- Aula 08 - [Redux](aula_08_context_e_redux.md);
- Aula 09 - [Mais React Navigation e React Native Elements](aula_09_serious_navigation.md);
- Aula 10 - [Autenticação](aula_10_auth.md);
- Aula 11 - [Mapas e Notificações](aula_11_maps_e_notifications.md);
- Aula 12 - [Publicação de Aplicativos](aula_12_bare_workflow_e_publishing.md);
.
