# Entrega dos exercícios

- **Grupo**: ds151-2023-1-t
- **Última atualização**: qui 22 jun 2023 14:13:43 -03

|Nome|	ds151-react-vote-assignment<br>2023-06-22|	ds151-task-list-assignment<br>2023-06-22|	ds151-tmdb-assignment<br>2023-06-22|	ds151-notes-assignment<br>2023-06-22|	ds151-toktok-game-test<br>2023-05-18|	ds151-gitlab-assignment<br>2023-06-22|
|----|	:---:|	:---:|	:---:|	:---:|	:---:|	:---:|
|ALLANA_DE_ALMEIDA_FLAUSINO|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork, mas nenhum commit até data de entrega|
|AMANDA_DE_MEDEIROS_ZEPECHOUKA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|CHRISTIAN_KENNEDY_DANIEL_MARTINS|	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|
|EDUARDO_ALUIZIO_BELLO|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork não encontrado |
|FERNANDO_BOMBARDELLI|	 Fork não encontrado |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|GEOVANNA_ALBERTI_CORREIA_DE_FREITAS|	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |
|GIANLUCA_NOTARI_MAGNABOSCO_DA_SILVA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|GISELE_GOMES_DA_SILVA|	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|GUILHERME_CICARELLO_GIRATA|	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork não encontrado |	 ok |	 ok |
|GUILHERME_PENNA_MORO|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |
|GUSTAVO_SCHWANKA|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |
|JACKSON_MARDI|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|JEFFERSON_SANTANA_FILHO|	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |
|JOAO_PEDRO_MARTINS_DE_PAULA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|JOÃO_VITOR_ANDRADE_DA_SILVA|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|LAISA_CRISTINA_KROLIKOVSKI_DA_SILVA|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|LEONARDO_DA_SILVA_ONEDA|	 ok |	 ok |	 Fork não encontrado |	 ok |	 Fork não encontrado |	 ok |
|LEONARDO_VZOREK|	 ok |	 ok |	 ok |	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|
|LUCAS_DIAS|	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |
|LUCAS_MACHADO|	 Fork não encontrado |	 Fork não encontrado |	 ok |	 ok |	 Fork não encontrado |	 Fork não encontrado |
|MARCO_ANTONIO_LAMPERT_JUNKES|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|MARINA_NEVES_BEPPLER|	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork não encontrado |
|MATHEUS_CORREA_FIORI|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
|PEDRO_ROMERO_MASSEDO|	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|RAFAELA_TREVISAN|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|RENAN_CORDEIRO_RAMOS|	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|RENAN_KENJI_KOGA_KURODA|	 ok |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|THALYSON_BRUCK_ANDREATTA|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork não encontrado |	 ok |	 Fork, mas nenhum commit até data de entrega|
|THIAGO_DOS_SANTOS|	 ok |	 Fork, mas nenhum commit até data de entrega|	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 ok |
|VICTOR_HUGO_FARIAS|	 Fork, mas nenhum commit até data de entrega|	 ok |	 Fork, mas nenhum commit até data de entrega|	 ok |	 ok |	 Fork, mas nenhum commit até data de entrega|
|VINÍCIUS_PACHECO|	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |	 Fork não encontrado |
|VINICIUS_RATZKE_SERVELO|	 ok |	 ok |	 ok |	 ok |	 ok |	 ok |
