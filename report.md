# Entrega dos exercícios

- **Grupo**: ds151-2023-1-t
- **Última atualização**: qui 13 abr 2023 16:16:02 -03

|Nome|	ds151-react-vote-assignment<br>2023-04-13|
|----|	:---:|
|ALLANA_DE_ALMEIDA_FLAUSINO|	 Fork, mas nenhum commit até data de entrega|
|AMANDA_DE_MEDEIROS_ZEPECHOUKA|	 Fork não encontrado |
|CHRISTIAN_KENNEDY_DANIEL_MARTINS|	 Fork não encontrado |
|EDUARDO_ALUIZIO_BELLO|	 ok |
|FERNANDO_BOMBARDELLI|	 Fork não encontrado |
|GEOVANNA_ALBERTI_CORREIA_DE_FREITAS|	 ok |
|GIANLUCA_NOTARI_MAGNABOSCO_DA_SILVA|	 ok |
|GISELE_GOMES_DA_SILVA|	 Fork não encontrado |
|GUILHERME_CICARELLO_GIRATA|	 ok |
|GUILHERME_PENNA_MORO|	 ok |
|GUSTAVO_SCHWANKA|	 Fork não encontrado |
|JACKSON_MARDI|	 Fork não encontrado |
|JEFFERSON_SANTANA_FILHO|	 Fork não encontrado |
|JOAO_PEDRO_MARTINS_DE_PAULA|	 ok |
|JOÃO_VITOR_ANDRADE_DA_SILVA|	 ok |
|LAISA_CRISTINA_KROLIKOVSKI_DA_SILVA|	 Fork não encontrado |
|LEONARDO_DA_SILVA_ONEDA|	 ok |
|LEONARDO_VZOREK|	 ok |
|LUCAS_DIAS|	 ok |
|LUCAS_MACHADO|	 Fork não encontrado |
|MARCO_ANTONIO_LAMPERT_JUNKES|	 ok |
|MARINA_NEVES_BEPPLER|	 Fork não encontrado |
|MATHEUS_CORREA_FIORI|	 Fork não encontrado |
|PEDRO_ROMERO_MASSEDO|	 ok |
|RAFAELA_TREVISAN|	 Fork não encontrado |
|RENAN_CORDEIRO_RAMOS|	 Fork não encontrado |
|RENAN_KENJI_KOGA_KURODA|	 ok |
|THALYSON_BRUCK_ANDREATTA|	 ok |
|THIAGO_DOS_SANTOS|	 ok |
|VICTOR_HUGO_FARIAS|	 Fork, mas nenhum commit até data de entrega|
|VINÍCIUS_PACHECO|	 Fork não encontrado |
|VINICIUS_RATZKE_SERVELO|	 ok |
