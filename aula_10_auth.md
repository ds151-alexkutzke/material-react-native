# Aula 10 - Autenticação

Consulte a video aula gravada e os materiais de leitura abaixo:

- [Sobre a API do Gitlab](https://gitlab.com/help/api/README.md);
- [AsyncStorage](https://react-native-async-storage.github.io/async-storage/docs/install/);
- [Expo Authentication](https://docs.expo.io/guides/authentication);
- Para saber mais sobre autenticação:
  - [JWT](https://jwt.io/)
  - [OAuth2](https://oauth.net/2/);

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-10-auth

