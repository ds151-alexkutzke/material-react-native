# Aula 05 - Layout

Consulte a video aula gravada e os materiais de leitura abaixo:

- [Propriedade `style`](https://reactnative.dev/docs/style);
- [Altura e Largura](https://reactnative.dev/docs/height-and-width);
- [Layout com Flexbox](https://reactnative.dev/docs/flexbox);
- [Guia para Flexbox](https://origamid.com/projetos/flexbox-guia-completo/);
- [Mais sobre Flexbox](https://developer.mozilla.org/pt-BR/docs/Learn/CSS/CSS_layout/Flexbox);
- [Yoga Layout Playground](https://yogalayout.com/playground/);
- [Yoga Layout Docs about Flexbox](https://yogalayout.com/docs);
- [Stackoverflow sobre flexGrow, flexShrink e flexBasis](https://stackoverflow.com/a/43147710);

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-05-layout

