# Aula 02 - React

Consulte a video aula gravada e os materiais de leitura abaixo:

- [Sobre o React](https://react.dev/learn):
- [Babel](https://babeljs.io/);
- [Utilizando o Create React App](https://create-react-app.dev/docs/getting-started);
- [Sobre hooks](https://reactjs.org/docs/hooks-intro.html):
  - [Usando o `setState`](https://reactjs.org/docs/hooks-state.html);
- [Pensando em React](https://react.dev/learn/thinking-in-react);
- [States com base em valores anteriores](https://javascript.plainenglish.io/commons-mistakes-with-react-usestate-hook-and-how-to-prevent-them-43c811ca7451).

## Passo a passo

- O que é o react?
  - Framework JS para criação de interfaces de usuário;
  - Declarativa, component-based e learn once, write anywhere;

Para compreender o React em sua essência, vamos criar um projeto bastante
básico com a biblioteca.

- Crie uma nova pasta chamada `teste-react`;
- Crie um novo arquivo com html básico;
- Adicione uma `div` com `id="react"`;

Seguiremos algo parecido com o guia [Add React to a Website](https://reactjs.org/docs/add-react-to-a-website.html) da
documentação oficial.

- Adicione as CDNs do  `react` e `react-dom` e um arquivo JS chamado `src/teste-react.js`.

```html
  <script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
  <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>
  <script src="src/teste-react.js"></script>
```

Ao final, seu aquivo HTML deve ficar similar ao seguinte:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Teste React</title>
</head>
<body>
  <div id="react"></div>

  <script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
  <script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>
  <script src="src/teste-react.js"></script>
</body>
</html>
```

- No arquivo `src/teste-react.js`, adicione o seguinte:

```js
'use strict';

const el = React.createElement;

function MeuElemento() {
  return(
    el('h1', {}, 'ola mundo em react') //3 argumentos: html, atributos, conteudo
  )
}

const domContainer = document.querySelector('#react');
ReactDOM.render(el(MeuElemento, null, null), domContainer);
```

Elementos criados pelo método `React.createElement` recebem 3 argumentos:
- Função que produz o elemento (html);
- Atributos que o elemento receberá;
- Conteúdo da tag;

É possível utilizar o segundo argumento para passar propriedades (`props`) para o elemento.
- `props` são valores recebidos por um componente e que não podem ser alterados.

```js
'use strict';

const el = React.createElement;

function MeuElemento(props) {
  return(
    el('h1', {}, props.content) //3 argumentos: html, atributos, conteudo
  )
}

const domContainer = document.querySelector('#react');
ReactDOM.render(el(MeuElemento, {content: 'agora com conteudo'}, null), domContainer);
```

Escrever componentes através do método `React.createElement` funciona, mas dá muito trabalho.

Seria muito mais interessante se pudéssemos escrever código HTML diretamente dentro do nosso
JS. Para que isso ocorra, podemos utilizar algo chamado `jsx`.

A biblioteca Babel auxilia nesse processo. Vale acessar o site para entender
melhor o que biblioteca faz.

https://babeljs.io/repl

Poderíamos apenas adicionar
a CDN dessa biblioteca, mas, como estamos trabalho em arquivo local, sem servidor,
teríamos problemas com CORS.

Então, instalaremos a biblioteca Babel para utilizando o `npm`.

```bash
npm init -y # projeto npm
npm install babel-cli babel-preset-react-app@3
npx babel --watch src --out-dir . --presets react-app/prod
```

O comando acima gera um novo arquivo JS. Logo, aponte o HTML para carregar o novo JS gerado.

Agora, podemos utilizar a sintaxe da extensão JSX. Isso permite a escrita de código
HTML que será automaticamente convertido para chamadas do método `React.createElement`:
- Importante: no JSX, `{}` é um bloco JS que será interpretado.

```js
'use strict';

const el = React.createElement;

function MeuElemento(props) {
  return(
    <h1>{props.content}</h1>
  )
}

const domContainer = document.querySelector('#react');
ReactDOM.render(<MeuElemento content="conteúdo com JSX" />, domContainer);
```

- Elementos funcionais e de classe;
- Exemplo de componente de classe:
   
```js
class MeuBotao extends React.Component {
  constructor(props){
    super(props);
  }
  
  render () { // precisa desse método
    return (
      <button>
        Clique aqui!
      </button>
    )
  }
}
```

- Geralmente, componentes funcionais são escritos com arrow functions.
- Utilizaremos componentes funcionais.
- Estado é uma informação que fica em observação. Se ele for alterado, o componente é redesenhado na tela.
- Exemplo de estado com componentes de classe:

```js

class MeuBotao extends React.Component {
  constructor(props){
    super(props);
    this.state = {clicked: false};
  }
  
  render () {
    if(this.state.clicked === true)
      return('Clicou!');
    
    return (
      <button onClick={() =>{
        this.setState({clicked: true})
        console.log('clicou');
        }
      } // setState vem do extends da classe
      > 
        Clique aqui!
      </button>
    )
  }
}


```

- `onClick` é o `onclick` do html, precisamos do cammel case no `jsx`.
- Tentar exemplo com vários botões: cada um mantem seu estado.

```js
ReactDOM.render(
  <div>
    <MeuBotao />
    <MeuBotao />
    <MeuBotao />
  </div>, domContainer);
```

- Props não podem ser alterados. Estados sim.
- Hooks são utilizados para estados (e outras coisas) em componentes funcionais.
- Hooks já são o padrão das novas aplicações React.

```js
// componente funcional com use state
const MeuElemento = (props) => {
  const [clicked, setClicked] = React.useState(false);
  if (clicked)
    return("Clicado com Hook");

  return (
    <button onClick={() => {
      setClicked(true);
    }} >
      Clique aqui!
    </button>
  )
}
```

- `useState` retorna variável e função para sua alteração:
  - JS agora permite atribuição dupla.

- `Create react app` para criação de uma estrutura pronta para desenvolver React.

```bash
npx create-react-app ds151-aula2 --use-npm
```

- Criar um novo arquivo `MeuBotao.js` colocar o código do componente funcional e os imports:

```js
import React, {useState} from 'react';

const MeuBotao = (props) => {
  const [clicked, setClicked] = React.useState(false);
  if (clicked)
    return("Clicado com Hook");

  return (
    <button onClick={() => {
      setClicked(true);
    }} >
      Clique aqui!
    </button>
  )
}

export default MeuBotao;
```

- Altere o arquivo `App.js`:

```js
import logo from './logo.svg';
import './App.css';
import MeuBotao from './MeuBotao';


function App() {
  return (
    <div className="App">
      <MeuBotao />
      <MeuBotao />
      <MeuBotao />
      <MeuBotao />
    </div>
  );
}

export default App;
```

- Inicie a aplicação com:

```bash
npm start
```

- Adicionar o componente criado no arquivo App.js (não esquecer imports);

- Fazer um build para gera aplicação final:

```bash
npm run build
```

- Verifique o conteúdo da pasta build;

- E o react native? Analise o arquivo `App.js` do `AwesomeProject` da aula passada:
  - React native é react. Mas com componentes específicos. Serão convertidos para componentes nativos.

## Tutoriais interessantes
- [Intro to React](https://reactjs.org/tutorial/tutorial.html);
- [React Tutorial: An Overview and Walkthrough](https://www.taniarascia.com/getting-started-with-react/) por Tania Rascia;
- [Build a CRUD App in React with Hooks](https://www.taniarascia.com/crud-app-in-react-with-hooks/) por Tania Rascia;

## Código fonte da video aula

https://gitlab.com/ds151-alexkutzke/code-02-react
