# Aula 11 - Mapas e Notificações

Consulte a video aula gravada e os materiais de leitura abaixo:

- [MapView](https://docs.expo.io/versions/latest/sdk/map-view/);
- [React Native Maps](https://github.com/react-native-maps/react-native-maps);
- [Expo Location](https://docs.expo.io/versions/latest/sdk/location/);
- [Expo Permissions](https://docs.expo.io/versions/v40.0.0/sdk/permissions/);
- [Expo Notifications](https://docs.expo.io/versions/latest/sdk/notifications/);
- [Expo Push Notifications](https://docs.expo.io/push-notifications/sending-notifications/);
- [Expo Push Notifications Tool](https://expo.io/notifications);

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-11-maps-notifications

