# Aula 08 - Context Genérico e Redux

Consulte a video aula gravada e os materiais de leitura abaixo:

## Código fonte da video aula

Consulte o README.md do repositório abaixo para instruções de instalação.

https://gitlab.com/ds151-alexkutzke/code-08-context-redux

## Contexto Genérico

- [Commit: 63d4834b](https://gitlab.com/ds151-alexkutzke/code-08-context-redux/-/commit/63d4834b1493f2b42326626020b57e728bb491b4): Cria context genérico;

## Bounded Actions

- [Commit: d11a28b2](https://gitlab.com/ds151-alexkutzke/code-08-context-redux/-/commit/d11a28b21165a9314ed2f0ed321340d25eb838c0): torna o contexto mais genérico, adicionando bounded actions;

## Redux

- [Commit: b307cb0b](https://gitlab.com/ds151-alexkutzke/code-08-context-redux/-/commit/b307cb0be159d08f7575acb62349bbc27b87c290): implementa contador com Redux;

```bash
npm install redux react-redux
```

- [Redux](https://react-redux.js.org/):
  - [Redux com hooks](https://react-redux.js.org/next/api/hooks#using-hooks-in-a-react-redux-app);
  - [Exemplo](https://blog.crowdbotics.com/use-redux-hooks-in-react-native-app/).
  - [Outro Exemplo](https://thoughtbot.com/blog/using-redux-with-react-hooks).

## Sobre State Management

- [React State Management in 2022](https://medium.com/@pitis.radu/react-state-management-in-2022-345c87922479);
